def main():
    print("Hello, this is a simple standalone application created by Mohammed Afzal- WDGET2024074.")
    
    # test if selenium is installed
    try:
        import selenium
        print("Selenium is installed")
    except ImportError:
        print("Selenium is not installed")

if __name__ == "__main__":
    main()
