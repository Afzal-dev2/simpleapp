# GitLab CI/CD Pipeline with Jenkins Integration

This project demonstrates how to set up a GitLab CI/CD pipeline to trigger a Jenkins build using the `/build` API provided by Jenkins. After every commit made to this repository, the GitLab CI/CD pipeline will automatically trigger a Jenkins build.

## Table of Contents

- [GitLab CI/CD Pipeline with Jenkins Integration](#gitlab-cicd-pipeline-with-jenkins-integration)
- [Project Structure](#project-structure)
- [Getting Started](#getting-started)
- [Workflow](#workflow)
- [Usage](#usage)
- [Screenshots](#screenshots)

## Project Structure

The repository contains the following files:

- `Jenkinsfile`: This file defines the Jenkins pipeline configuration.
- `app.py`: Sample Python application file.
- `requirements.txt`: List of dependencies required by the application.
-  `.gitlab-ci.yml`: to trigger jenkins build 

## Getting Started

To set up the integration between GitLab and Jenkins, follow these steps:

1. Create a pipeline project in Jenkins.
2. Provide the GitLab repository URL during project creation.
3. Enable the "Poll SCM" option in the project configuration.
4. Save the project configuration.

## Workflow

Once the integration is set up, the workflow is as follows:

1. Make a commit to the GitLab repository.
2. The GitLab CI/CD pipeline will be triggered automatically.
3. The pipeline will make an API request to the Jenkins `/build` endpoint, triggering a Jenkins build.

## Usage

To use this project, follow these steps:

1. Clone the repository to your local machine.
2. Make any necessary changes to the `app.py` file or add new dependencies to `requirements.txt` and change the jenkins server name, project name and token name present in the url found in .gitlab-ci.yml.
3. Commit and push your changes to the GitLab repository.
4. The GitLab CI/CD pipeline will be triggered, which will in turn trigger a Jenkins build.

## Screenshots

![screenshot1](/screenshots/jenkins_screenshot.png)
![screenshot2](/screenshots/screenshot2.png)